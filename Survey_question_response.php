<?php

namespace ACSP\Model\CMS;

class Survey_question_response extends \Model\CMS\Survey_question_response {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'answer' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'survey_question_answer_id',
            'model' => '\ACSP\Model\CMS\Survey_question_answer'
        ],
        'question' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'survey_question_id',
            'model' => '\ACSP\Model\CMS\Survey_question'
        ],
    ];

}

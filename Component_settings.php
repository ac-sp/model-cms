<?php

namespace ACSP\Model\CMS;

class Component_settings extends \Model\CMS\Component_settings {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component'
        ],
        'page_component_settings' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_settings_id',
            'model' => '\ACSP\Model\CMS\Page_component_settings'
        ],
    ];

}

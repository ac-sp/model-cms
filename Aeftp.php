<?php

namespace ACSP\Model\CMS;

class Aeftp {
    
    use \doctrine\Dashes\Model,
            \acsp\helpers\core\Model,
            \Model\CMS\Component_datatype_behavior {
        \acsp\helpers\core\Model::loadModelInstance insteadof \doctrine\Dashes\Model;
    }

    const server = "ftp.ae.com.br";
    const user = "aeftp285";
    const pass = "ft*285&77";
    const conn_fail = 1;
    const auth_fail = 2;
    const buffer_fail = 3;
    const file_xml_read_fail = 1;
    const file_download_fail = 2;

    const dir = '../public/upload/aeftp';

    protected $conn = NULL;
    public $errorList = [];

    public function connect($reconnect = false) {
        if ($this->conn && !$reconnect) {
            return $this->conn;
        }

        $auth = null;
        $conn = ftp_connect(self::server);
        $conn ? ($auth = ftp_login($conn, self::user, self::pass)) : $this->errorList['global'] = self::conn_fail;

        if (!($conn && $auth)) {
            $this->errorList['global'] = self::auth_fail;
            return false;
        }

        return $this->conn = $conn;
    }

    public function disconnect() {
        @ftp_close($this->conn);
    }

    public function getExchange() {
        $file = "cotacoes.xml";
        $folder_path = $this->getDir();
        $local_file = $folder_path . $file;

        $xml = simplexml_load_file($local_file, 'SimpleXMLElement', LIBXML_NOCDATA);
        return json_decode(json_encode((array) $xml), TRUE);
    }

    public function getFtpExchange() {

        if ($this->connect() === false) {
            return false;
        }
        
        $file = 'cotacao.xml';

        $fileGetResult = $this->getFtpItem($file, 'cotacoes.xml');
        $fileGetResult!==true && ($this->errorList['file'][$file] = $content);

        $this->disconnect();
        return true;
    }

    public function getFtpList() {

        if ($this->connect() === false) {
            return false;
        }
        
        $this->removeOldFiles();
        $files = @ftp_nlist($this->conn, ".");
        
        if(!$files) {
            $this->errorList['global'] = self::buffer_fail;
        } else {
        
            rsort($files);

            foreach ($files as $k => $file) {
                if ($file == 'cotacao.xml' || $file == 'cotacoes.xml') {
                    continue;
                }

                $isOldFile = $this->detectOldArticle($file);
                if ($isOldFile) {
                    $this->_delete_file($file);
                } else {
                    $fileGetResult = $this->getFtpItem($file);
                    $fileGetResult!==true && ($this->errorList['file'][$file] = $fileGetResult);
                }
            }
            
        }

        $this->disconnect();
        return $this->getListContents();
    }
    
    public function detectOldArticle($file) {
        $day = str_replace('.', '-', substr($file, 7, 10));

        if (preg_match('/EP/', $file) || $this->_diff_date($day) >= 2) {
            return true;
        }
        return false;
    }
    
    public function removeOldFiles() {
        $files = $this->getLocalArticleList();
        
        foreach ($files as $k => $file) {
            if ($this->detectOldArticle($file)) {
                $this->_delete_file($file);
            }
        }
    }

    public function getLocalArticleList() {
        $folder_path = $this->getDir();
        $files = array_map(function($filepath) {
            return preg_replace('/^.*\//', '', $filepath);
        }, glob($folder_path . '*.xml'));
        sort($files);
        
        foreach ($files as $k => $file) {
            if ($file == 'cotacao.xml' || $file == 'cotacoes.xml') {
                unset($files[$k]);
            }
        }

        return $files;
    }
    
    public function getListContents() {
        $files = $this->getLocalArticleList();
        $contents = [];

        foreach ($files as $k => $file) {

            if ($file == 'cotacao.xml' || $file == 'cotacoes.xml') {
                continue;
            }

            $day = str_replace('.', '-', substr($file, 7, 10));

            $content = $this->getXmlData($file);
            is_array($content) ? ($contents[] = $content) : ($this->errorList['file'][$file] = $content);
        }

        return $contents;
    }

    /**
     * Esta função calcula a diferença entre a data corrente e a data do arquivo que está no ftp
     * @param d1 date
     * @param d2 date
     * @return int
     */
    public function _diff_date($d1, $d2 = NULL) {
        !$d2 && ($d2 = date('Y-m-d'));

        $date1 = $d1 . ' 00:00:00';
        $date2 = $d2 . ' 00:00:00';

        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $seconds_diff = $ts2 - $ts1;

        return floor($seconds_diff / 3600 / 24);
    }

    /**
     * Esta função verifica se o arquivo que está no ftp já foi copiado para a pasta webroot/xml, caso não tenha sido, esta função copia o arquivo para a pasta
     * Em seguida o arquivo XML é aberto a lido, para que as suas informações sejam extraídas.
     * @param file string
     * @return array
     */
    public function getFtpItem($file, $localName = null) {
        empty($localName) && ($localName = $file);

        $folder_path = $this->getDir();
        $local_file = $folder_path . $localName;
        $server_file = $file;

        $fileGetFail = $this->connect() !== false && !$this->downloadFile($local_file, $server_file) && !is_file($local_file);
        if ($fileGetFail) {
            return self::file_download_fail;
        }
        return true;
    }

    public function getXmlData($file, $getAllData = false) {
        $folder_path = $this->getDir();
        $local_file = $folder_path . $file;

        if ($xml = @simplexml_load_file($local_file)) {
            $item = [];

            $item['id'] = base64_encode($file);
            $item['file'] = $file;
            $item['tit'] = (string) $xml->TITULO;
            $item['edit'] = (string) $xml->EDITORIA;

            $data = trim((string) $xml->DATA);
            $hora = trim((string) $xml->HORA);

            $date = !empty($data) && preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $data) ? implode('-', array_reverse(explode('/', $data))) : '0000-00-00';
            $time = !empty($hora) && preg_match('/^\d{2}\:\d{2}$/', $hora) ? $hora : '00:00';

            $item['data'] = $data . ' ' . $hora;
            $item['datetime'] = $date . ' ' . $time;

            if ($getAllData) {
                $item['ator'] = (string) $xml->AUTOR;
                $item['retranca'] = (string) $xml->RETRANCA;
                $item['keywords'] = (string) $xml->KEYWORDS;
                $item['local'] = (string) $xml->LOCAL;
                $item['artigo'] = (string) $xml->INTEGRA;
            }

            return $item;
        } else {
            return self::file_xml_read_fail;
        }
    }

    public function get($id) {
        $file = base64_decode($id);

        return $this->getXmlData($file, true);
    }

    public function getDir() {
        return APPPATH . self::dir . '/';
    }

    public function downloadFile($local_file, $server_file) {
        @unlink($local_file);
        
        $url = 'ftp://'.self::user.':'.self::pass.'@'.self::server.'/'.$server_file;
//        printf('<pre>%s</pre>', var_export($url, true));
        $contents = @file_get_contents($url);
        if ($contents !== false && file_put_contents($local_file, $contents)) {
//        if (ftp_get($this->conn, $local_file, $server_file, FTP_BINARY)) {
//            echo "Successfully written to $local_file\n";
            return true;
        }
    }

    public function delete($id) {
        $item = $this->get($id);
        $result = $this->_delete_file($item['file']);

        return $result;
    }

    /**
     * Esta função apaga o arquivo que já foi copiado para a pasta webroot/xml e o arquivo que se encontra no ftp da agência Estado
     * @param file string
     * @return void
     */
    public function _delete_file($file) {
        $folder_path = $this->getDir();
        $local_file = $folder_path . $file;
        $server_file = $file;
        
        if(is_file($local_file)) {
            @copy($local_file, $local_file . '.bak');
            @unlink($local_file);
        }
        if (ENVIRONMENT === 'production') {
//            there no reason to delete the remote file
//            $this->connect() && (@ftp_delete($this->conn, $file));
        }

        return true;
    }

    public $datatypeExchangeData = [];

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this));

        if (empty($this->datatypeExchangeData)) {
            $this->datatypeExchangeData = $this->getExchange();
        }

        $this->datatypeExchangeData['ATIVO'] = \Crush\Collection::transform($this->datatypeExchangeData['ATIVO'], 'COD');

        $list = (array) @$this->datatypeExchangeData['ATIVO'][$mySets['pair']];
        !empty($_GET['debug_exchange']) && printf('<pre>%s</pre>', var_export($list, true));
        return $list;
    }

}

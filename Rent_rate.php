<?php

namespace ACSP\Model\CMS;

class Rent_rate {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'rent_rate',
        'foreignKeys' => [
            'header' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'rent_header_id',
                'model' => '\Model\CMS\Rent_header'
            ],
            'type' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'rent_type_id',
                'model' => '\Model\CMS\Rent_type'
            ],
        ],
    ];

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = \HBasis\NORELATED) {
        unset($conditions['status']);
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, explode('_',\Crush\Basic::getClassShortName($this))[0]);

        $criteria = [];
        $this->loadModelInstance($this->getAttr('foreignKeys')['header']['model']);
        $this->loadModelInstance($this->getAttr('foreignKeys')['type']['model']);

        $mySets['limit'] = 999;

        $results = \Crush\Collection::transform($this->execCommonFind($this, $criteria, $categoryId, $mySets), 'rent_type_id', [], ['array']);
        $headerList = $this->model['Rent_header']->getListBy();
        $typeList = $this->model['Rent_type']->getListBy();

        $list = ['rate' => $results, 'type' => $typeList, 'header' => $headerList];
        !empty($_GET['debug_rent']) && printf('<pre>%s</pre>', var_export($list, true)) && die();
        return $list;
    }

}

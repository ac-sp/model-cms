<?php

namespace ACSP\Model\CMS;

class Announce_tag extends \Model\CMS\Announce_tag {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'announce' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'announce_id',
            'model' => '\ACSP\Model\CMS\Announce'
        ],
        'tag' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Tag'
        ],
    ];

}

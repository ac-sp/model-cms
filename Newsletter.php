<?php

namespace ACSP\Model\CMS;
/* used to test dashes and codeblaze implementations side by side */
class Newsletter {
    
    use \doctrine\Dashes\Model,
        \acsp\helpers\core\Model {
        \acsp\helpers\core\Model::loadModelInstance insteadof \doctrine\Dashes\Model;
        \doctrine\Dashes\Model::create as _create;
    }

    protected $table = 'newsletter';
    protected $foreignKeys = [
    ];
    protected $fieldsFormat = [
        'created' => ':',
    ];

    public function create($data) {
        $existent = $this->getBy(['email'=>$data['email']], [$this->getAttr('primaryKey')]);
        if(!empty($existent)) {
            return $this->update($existent[$this->getAttr('primaryKey')], $data);
        }
        
        return $this->_create($data);
    }

}

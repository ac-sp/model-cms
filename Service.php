<?php

namespace ACSP\Model\CMS;

class Service extends \Model\CMS\Service {

    use \acsp\helpers\core\Model,
            \codeigniter\CodeBlaze\Serialize;

    public $foreignKeys = [
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'service_id',
            'model' => '\Model\CMS\Service_param'
        ],
        'tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'service_id',
            'model' => '\ACSP\Model\CMS\Service_tag'
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }

}

<?php

namespace ACSP\Model\CMS;

class Publisher extends \Model\CMS\Publisher {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'post' => [
            'type' => \HBasis\HASMANY,
            'key' => 'publisher_id',
            'model' => '\ACSP\Model\CMS\Post'
        ],
        'tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'publisher_id',
            'model' => '\ACSP\Model\CMS\Publisher_tag'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'publisher_id',
            'model' => '\Model\CMS\Publisher_param'
        ],
    ];
    
    const picFolder = 'public/upload/publisher/';
}

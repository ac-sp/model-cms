<?php

namespace ACSP\Model\CMS;

class Menu extends \Model\CMS\Menu {
    
    use \acsp\helpers\core\Model;

    public $foreignKeys = [
        'parent' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'menu_id',
            'model' => '\ACSP\Model\CMS\Menu'
        ],
        'child' => [
            'type' => \HBasis\HASMANY,
            'key' => 'menu_id',
            'model' => '\ACSP\Model\CMS\Menu',
            'order' => 'order ASC'
        ],
        'page' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_id',
            'model' => '\ACSP\Model\CMS\Page'
        ],
        'post' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'post_id',
            'model' => '\ACSP\Model\CMS\Post'
        ],
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'menu_id',
            'model' => '\Model\CMS\Menu_param'
        ],
    ];

}

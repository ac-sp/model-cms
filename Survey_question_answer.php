<?php

namespace ACSP\Model\CMS;

class Survey_question_answer extends \Model\CMS\Survey_question_answer {

    use \acsp\helpers\core\Model;
    
    public function addVote($id) {
        $answer = $this->get($id);
        $answer['votes'] = (int) $answer['votes'] + 1;
        return $this->update($id, $answer);
    }

}

<?php

namespace ACSP\Model\CMS;

class Survey_question extends \Model\CMS\Survey_question {

    use \acsp\helpers\core\Model,
            \codeigniter\CodeBlaze\Serialize;

    protected $foreignKeys = [
        'answer' => [
            'type' => \HBasis\HASMANY,
            'key' => 'survey_question_id',
            'model' => '\ACSP\Model\CMS\Survey_question_answer'
        ],
        'survey' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'survey_id',
            'model' => '\ACSP\Model\CMS\Survey'
        ]
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }

}

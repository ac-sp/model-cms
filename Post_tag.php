<?php

namespace ACSP\Model\CMS;

class Post_tag extends \Model\CMS\Post_tag {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'post' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'post_id',
            'model' => '\ACSP\Model\CMS\Post'
        ],
        'tag' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Tag'
        ],
    ];

}

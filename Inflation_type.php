<?php

namespace ACSP\Model\CMS;

class Inflation_type {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'inflation_type',
        'foreignKeys' => [
            'rate' => [
                'type' => \HBasis\HASMANY,
                'key' => 'inflation_type_id',
                'model' => '\Model\CMS\Inflation_rate'
            ],
        ],
    ];

    public function getListBy($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $orderby = 'order ASC';
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

}

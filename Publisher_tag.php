<?php

namespace ACSP\Model\CMS;

class Publisher_tag extends \Model\CMS\Publisher_tag {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'publisher' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'publisher_id',
            'model' => '\ACSP\Model\CMS\Publisher'
        ],
        'tag' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Tag'
        ],
    ];

}

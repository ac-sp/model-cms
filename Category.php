<?php

namespace ACSP\Model\CMS;

class Category extends \Model\CMS\Category {

    use \acsp\helpers\core\Model;

    public $foreignKeys = [
        'parent' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'owner' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_owner_id',
            'model' => '\ACSP\Model\CMS\Category_owner'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'category_id',
            'model' => '\Model\CMS\Category_param'
        ],
    ];

}

<?php

namespace ACSP\Model\CMS;

class Video_tag extends \Model\CMS\Video_tag {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'video' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'video_id',
            'model' => '\ACSP\Model\CMS\Video'
        ],
        'tag' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Tag'
        ],
    ];

}

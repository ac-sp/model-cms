<?php

namespace ACSP\Model\CMS;

class Rent_header {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'rent_header',
        'foreignKeys' => [
            'rate' => [
                'type' => \HBasis\HASMANY,
                'key' => 'rent_header_id',
                'model' => '\Model\CMS\Rent_rate'
            ],
        ],
    ];

    public function getListBy($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $orderby = 'order ASC';
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

}

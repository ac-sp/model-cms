<?php

namespace ACSP\Model\CMS;

class Page_component extends \Model\CMS\Page_component {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'page_block' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_block_id',
            'model' => '\ACSP\Model\CMS\Page_block'
        ],
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component'
        ],
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'page_component_settings' => [
            'type' => \HBasis\HASMANY,
            'key' => 'page_component_id',
            'model' => '\ACSP\Model\CMS\Page_component_settings'
        ],
    ];

}

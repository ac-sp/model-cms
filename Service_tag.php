<?php

namespace ACSP\Model\CMS;

class Service_tag extends \Model\CMS\Service_tag {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'service' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'service_id',
            'model' => '\ACSP\Model\CMS\Service'
        ],
        'tag' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Tag'
        ],
    ];

}

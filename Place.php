<?php

namespace ACSP\Model\CMS;

class Place extends \Model\CMS\Place {

    use \acsp\helpers\core\Model;

    protected $foreignKeys = [
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
    ];

}

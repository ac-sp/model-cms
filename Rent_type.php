<?php

namespace ACSP\Model\CMS;

class Rent_type {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'rent_type',
        'foreignKeys' => [
            'rate' => [
                'type' => \HBasis\HASMANY,
                'key' => 'rent_type_id',
                'model' => '\ACSP\Model\CMS\Rent_rate'
            ],
        ],
    ];

    public function getListBy($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $orderby = 'order ASC';
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

}

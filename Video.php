<?php

namespace ACSP\Model\CMS;

class Video extends \Model\CMS\Video {

    use \acsp\helpers\core\Model,
            \codeigniter\CodeBlaze\Serialize;

    protected $foreignKeys = [
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'video_id',
            'model' => '\ACSP\Model\CMS\Video_tag'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'video_id',
            'model' => '\Model\CMS\Video_param'
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }

}

<?php

namespace ACSP\Model\CMS;

/* FEEDERS OF A COMPONENT */
class Component_datatype_component extends \Model\CMS\Component_datatype_component {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component'
        ],
        'datatype' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_datatype_id',
            'model' => '\ACSP\Model\CMS\Component_datatype'
        ],
    ];

}

<?php

namespace ACSP\Model\CMS;

class Page_block extends \Model\CMS\Page_block {
    
    use \acsp\helpers\core\Model,
            \acsp\helpers\Ci_Twig_Cms;
    
    public $foreignKeys = [
        'page' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_id',
            'model' => '\ACSP\Model\CMS\Page'
        ],
        'page_component' => [
            'type' => \HBasis\HASMANY,
            'key' => 'page_block_id',
            'model' => '\ACSP\Model\CMS\Page_component'
        ],
    ];

}

<?php

namespace ACSP\Model\CMS;

class Post extends \Model\CMS\Post {

    use \acsp\helpers\core\Model,
            \codeigniter\CodeBlaze\Serialize;

    public $foreignKeys = [
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'publisher' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'publisher_id',
            'model' => '\ACSP\Model\CMS\Publisher'
        ],
        'tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'post_id',
            'model' => '\ACSP\Model\CMS\Post_tag'
        ],
        'post_gallery' => [
            'type' => \HBasis\HASMANY,
            'key' => 'post_id',
            'model' => '\ACSP\Model\CMS\Post_gallery'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'post_id',
            'model' => '\Model\CMS\Post_param'
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }

}

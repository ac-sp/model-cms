<?php

namespace ACSP\Model\CMS;

class Inflation_rate {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'inflation_rate',
        'foreignKeys' => [
            'header' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'inflation_header_id',
                'model' => '\Model\CMS\Inflation_header'
            ],
            'type' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'inflation_type_id',
                'model' => '\Model\CMS\Inflation_type'
            ],
        ],
    ];

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = \HBasis\NORELATED) {
        unset($conditions['status']);
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, explode('_',\Crush\Basic::getClassShortName($this))[0]);

        $criteria = [];
        $this->loadModelInstance($this->getAttr('foreignKeys')['header']['model']);
        $this->loadModelInstance($this->getAttr('foreignKeys')['type']['model']);

        $mySets['limit'] = 999;

        $results = \Crush\Collection::transform($this->execCommonFind($this, $criteria, $categoryId, $mySets), 'inflation_type_id', [], ['array']);
        $headerList = \Crush\Collection::transform($this->model['Inflation_header']->getListBy(), 'id');
        $typeList = \Crush\Collection::transform($this->model['Inflation_type']->getListBy(), 'id');

        $list = ['rate' => $results, 'type' => $typeList, 'header' => $headerList];
        !empty($_GET['debug_inflation']) && printf('<pre>%s</pre>', var_export($list, true)) && die();
        return $list;
    }

}

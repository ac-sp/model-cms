<?php

namespace ACSP\Model\CMS;

class Announce extends \Model\CMS\Announce {

    use \codeigniter\CodeBlaze\Serialize,
        \acsp\helpers\core\Model;

    protected $foreignKeys = [
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'announce_id',
            'model' => '\ACSP\Model\CMS\Announce_tag'
        ],
        'param' => [
            'type' => \HBasis\HASMANY,
            'key' => 'announce_id',
            'model' => '\Model\CMS\Announce_param'
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }
}

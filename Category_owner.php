<?php

namespace ACSP\Model\CMS;

class Category_owner extends \Model\CMS\Category_owner {

    use \acsp\helpers\core\Model;

    public $foreignKeys = [
        'category' => [
            'type' => \HBasis\HASMANY,
            'key' => 'category_owner_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
        'page' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_id',
            'model' => '\ACSP\Model\CMS\Page'
        ]
    ];

}

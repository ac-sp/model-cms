<?php

namespace ACSP\Model\CMS;

class Survey extends \Model\CMS\Survey {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'question' => [
            'type' => \HBasis\HASMANY,
            'key' => 'survey_id',
            'model' => '\ACSP\Model\CMS\Survey_question'
        ],
        'category' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'category_id',
            'model' => '\ACSP\Model\CMS\Category'
        ],
    ];

}

<?php

namespace ACSP\Model\CMS;

class Page_component_settings extends \Model\CMS\Page_component_settings {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'page_component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_component_id',
            'model' => '\ACSP\Model\CMS\Page_component'
        ],
        'component_settings' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_settings_id',
            'model' => '\ACSP\Model\CMS\Component_settings'
        ],
    ];

}

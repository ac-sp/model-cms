<?php

namespace ACSP\Model\CMS;

class Page extends \Model\CMS\Page {

    use \acsp\helpers\core\Model,
            \acsp\helpers\Ci_Twig_Cms,
            \codeigniter\CodeBlaze\Serialize;

    public $foreignKeys = [
        'page' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'page_id',
            'model' => '\ACSP\Model\CMS\Page'
        ],
        'page_block' => [
            'type' => \HBasis\HASMANY,
            'key' => 'page_id',
            'model' => '\ACSP\Model\CMS\Page_block'
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->unserialize();
    }
    
    public function saveRoutesFile($filepath, $text) {
        $ci = \get_instance();
        $alias = $ci->config->config['systemAlias'];
        $filepath .= '.'.$alias;
        file_put_contents(APPPATH . 'config/' . $filepath . '.php', $text);
    }

}

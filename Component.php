<?php

namespace ACSP\Model\CMS;

class Component extends \Model\CMS\Component {

    use \acsp\helpers\core\Model,
            \acsp\helpers\Ci_Twig_Cms;
    
    public $foreignKeys = [
//        'category' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'category_id',
//            'model' => '\ACSP\Model\CMS\Category'
//        ],
//        'datatype' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'component_datatype_id',
//            'model' => '\ACSP\Model\CMS\Component_datatype'
//        ],
        'component_datatype_component' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component_datatype_component',
            'recursive' => \HBasis\HASMANY
        ],
        'component_settings' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component_settings'
        ],
        'component_script' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component_script'
        ],
        'page_component' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Page_component'
        ],
    ];
    
    public function getFeedMethod($datatypeFunction) {
        $function = explode('/', $datatypeFunction);
        
        $modelClass = $function[0];
        
        if(strpos($modelClass, '\\')===false && strpos($modelClass, '_model')===false) {
            $modelTemporarySplit = explode('_', $modelClass, 2);
            $modelClass = ucfirst($modelTemporarySplit[0]);
            if(count($modelTemporarySplit) > 1) {
                $function[1] = 'datatype_'.$modelTemporarySplit[1];
            }
            
            if(in_array($modelTemporarySplit[0], ['rent', 'inflation'])) {
                $modelClass .= '_rate';
            }
            $modelClass .= '';
        }
        
        if(!class_exists($modelClass)) {
            $otherModelClass = '\ACSP\Model\CMS\\'.$modelClass;
            if(class_exists($otherModelClass)) {
                $modelClass = $otherModelClass;
            } else {
                $otherModelClass = '\Model\CMS\\'.$modelClass;
                if(class_exists($otherModelClass)) {
                    $modelClass = $otherModelClass;
                }
            }
        }
        
        $model = $this->loadModelInstance($modelClass);
        $method = empty($function[1]) ? ('datatype') : $function[1];
        
        return [$model, $method];
    }

}

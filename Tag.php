<?php

namespace ACSP\Model\CMS;

class Tag extends \Model\CMS\Tag {

    use \acsp\helpers\core\Model;
    
    protected $foreignKeys = [
        'video_tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Video_tag'
        ],
        'post_tag' => [
            'type' => \HBasis\HASMANY,
            'key' => 'tag_id',
            'model' => '\ACSP\Model\CMS\Post_tag'
        ],
    ];

}

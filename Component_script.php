<?php

namespace ACSP\Model\CMS;

class Component_script extends \Model\CMS\Component_script {

    use \acsp\helpers\core\Model;
    
    public $foreignKeys = [
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\ACSP\Model\CMS\Component'
        ],
    ];

}

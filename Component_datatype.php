<?php

namespace ACSP\Model\CMS;

class Component_datatype extends \Model\CMS\Component_datatype {

    use \acsp\helpers\core\Model,
            \Model\CMS\Component_datatype_behavior;
    
    protected $foreignKeys = [
//        'category' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'category_id',
//            'model' => '\ACSP\Model\CMS\Category'
//        ],
        'component' => [
            'type' => \HBasis\HASMANY,
            'key' => 'component_datatype_id',
            'model' => '\ACSP\Model\CMS\Component'
        ]
    ];

}
